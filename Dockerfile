FROM adoptopenjdk/openjdk11-openj9:jdk-11.0.1.13-alpine-slim

WORKDIR /app

ENV JAVA_TOOL_OPTIONS=-Dfile.encoding=UTF-8

COPY target/scala-*/sats-ingestion.jar /app/bin/sats-ingestion

EXPOSE 8080
CMD java -jar /app/bin/sats-ingestion -Dhttp.port=8080
