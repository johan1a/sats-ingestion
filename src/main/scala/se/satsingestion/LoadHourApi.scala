package se.satsingestion

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.http.scaladsl.server.Directives.{path, _}
import com.typesafe.config.Config
import io.circe.generic.auto._
import io.circe.syntax.EncoderOps
import org.slf4j.Logger

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success}

class LoadHourApi(
    config: Config,
    dataRetriever: DataRetriever,
    loadHourService: LoadHourService
)(
    implicit val executionContext: ExecutionContext,
    val actorSystem: ActorSystem,
    implicit val logger: Logger
) {

  val statisticsCalculator: StatisticsCalculator = new StatisticsCalculator()(
    logger
  )

  val route = {
    concat(
      pathPrefix("api" / "sats") {
        concat(
          path("retrievedata") {
            put {
              logger.info("Data retrieval triggered by API")
              onComplete(dataRetriever.retrieveData()) { _ =>
                complete(
                  HttpEntity(ContentTypes.`text/html(UTF-8)`, "<h1>OK</h1>")
                )
              }
            }
          },
          path("hours") {
            get {
              onComplete(
                loadHourService
                  .list()
                  .map { result: Seq[LoadHour] =>
                    result.asJson.noSpaces
                  }) {
                case Success(json) =>
                  complete(HttpEntity(ContentTypes.`application/json`, json))
                case Failure(exception) =>
                  logger.error("Got exception", exception)
                  complete(StatusCodes.InternalServerError)
              }
            }
          },
          path("today") {
            get {
              onComplete(
                loadHourService
                  .listCurrentDay()
                  .map { result: Map[Int, Seq[LoadHour]] =>
                    result.asJson.noSpaces
                  }) {
                case Success(json) =>
                  complete(HttpEntity(ContentTypes.`application/json`, json))
                case Failure(exception) =>
                  logger.error("Got exception", exception)
                  complete(StatusCodes.InternalServerError)
              }
            }
          },
          path("hours" / IntNumber) { centerId =>
            get {
              onComplete(
                loadHourService
                  .list(centerId)
                  .map { result: Seq[LoadHour] =>
                    result.asJson.noSpaces
                  }) {
                case Success(json) =>
                  complete(HttpEntity(ContentTypes.`application/json`, json))
                case Failure(exception) =>
                  logger.error("Got exception", exception)
                  complete(StatusCodes.InternalServerError)
              }
            }
          },
          path("averages" / IntNumber) { centerId =>
            get {
              onComplete(loadHourService.list(centerId).map { result: Seq[LoadHour] =>
                statisticsCalculator
                  .calculateAverages(result)
                  .asJson
                  .noSpaces
              }) {
                case Success(json) =>
                  complete(HttpEntity(ContentTypes.`application/json`, json))
                case Failure(exception) =>
                  logger.error("Got exception", exception)
                  complete(StatusCodes.InternalServerError)
              }
            }
          }
        )
      },
      extractUnmatchedPath { remaining =>
        complete(s"Unmatched: '$remaining'")
      }
    )
  }

  private val interface = config.getString("http.interface")
  private val port = config.getInt("http.port")

  def bind(): Future[Http.ServerBinding] =
    Http().newServerAt(interface, port).bind(route)

}
