package se.satsingestion

import java.time.temporal.ChronoUnit
import java.time.{LocalDate, LocalDateTime, LocalTime}

import akka.actor.Scheduler
import com.typesafe.config.Config
import org.slf4j.Logger

import scala.concurrent.ExecutionContext
import scala.concurrent.duration.{FiniteDuration, MINUTES}

class DataRetrievalScheduler(
    config: Config,
    dataRetriever: DataRetriever,
    scheduler: Scheduler
)(implicit val executionContext: ExecutionContext, val logger: Logger) {

  private val startHour = config.getString("schedule.startHour").toInt
  private val startMinute = config.getString("schedule.startMinute").toInt
  private val startTime = LocalTime.of(startHour, startMinute)
  private var startDateTime = LocalDateTime.of(LocalDate.now, startTime)
  if (startDateTime.isBefore(LocalDateTime.now)) {
    startDateTime = startDateTime.plusDays(1)
  }
  private val initialDelay: FiniteDuration = FiniteDuration(
    ChronoUnit.MINUTES.between(LocalDateTime.now, startDateTime).toInt,
    MINUTES
  )
  private val delay: FiniteDuration = FiniteDuration(
    config.getDuration("schedule.delay").toMinutes.toInt,
    MINUTES
  )

  def schedule(): Unit = {
    logger.info(s"initialDelay: $initialDelay delay: $delay")
    scheduler.scheduleWithFixedDelay(initialDelay, delay)(task)
  }

  private val task = new Runnable {
    def run(): Unit = {
      dataRetriever.retrieveData()
    }
  }
}
