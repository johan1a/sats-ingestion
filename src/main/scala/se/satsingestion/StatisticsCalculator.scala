package se.satsingestion

import java.time.LocalDateTime

import org.slf4j.Logger

import scala.collection.mutable
import scala.collection.mutable.ListBuffer

class StatisticsCalculator()(logger: Logger) {

  private val dayOrder = Map(
    "Monday" -> 0,
    "Tuesday" -> 1,
    "Wednesday" -> 2,
    "Thursday" -> 3,
    "Friday" -> 4,
    "Saturday" -> 5,
    "Sunday" -> 6
  )

  case class StatisticalHour(hourNumber: Int, averageLoad: Float)

  case class StatisticalDay(dayOfWeek: String, hours: List[StatisticalHour])

  case class StatisticsResult(days: List[StatisticalDay])

  def calculateAverages(hours: Seq[LoadHour]): StatisticsResult = {
    val days = mutable.Map[String, mutable.Map[Int, ListBuffer[Float]]]()

    hours
      .filter(_.actualLoadFactor.isDefined)
      .foreach { hour =>
        val dayOfWeek = hour.date.getDayOfWeek.name().toLowerCase().capitalize
        if (!days.contains(dayOfWeek)) {
          days(dayOfWeek) = mutable.Map()
        }
        val dayMap = days(dayOfWeek)
        if (!dayMap.contains(hour.hourNumber)) {
          dayMap(hour.hourNumber) = ListBuffer()
        }
        dayMap(hour.hourNumber) += hour.actualLoadFactor.get
      }

    val result = days.keys
      .map { day =>
        val hours = days(day)
          .map { hour: (Int, ListBuffer[Float]) =>
            StatisticalHour(hour._1, hour._2.sum / hour._2.size)
          }
          .toList
          .sortBy { entry =>
            entry.hourNumber
          }
        StatisticalDay(day, hours)
      }
      .toList
      .sortWith { (first, second) =>
        dayOrder(first.dayOfWeek) < dayOrder(second.dayOfWeek)
      }

    StatisticsResult(result)
  }

}
