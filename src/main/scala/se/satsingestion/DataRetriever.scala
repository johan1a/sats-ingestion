package se.satsingestion

import com.typesafe.config.Config
import org.json4s.DefaultFormats
import org.json4s.native.Serialization
import org.slf4j.Logger
import sttp.client3.json4s.asJson
import sttp.client3.{SttpBackend, UriContext, basicRequest}

import scala.collection.convert.ImplicitConversions.`collection AsScalaIterable`
import scala.concurrent.{ExecutionContext, Future}

class DataRetriever(
    config: Config,
    backend: SttpBackend[Future, Any],
    loadHourService: LoadHourService
)(implicit val executionContext: ExecutionContext, val logger: Logger) {

  implicit val serialization: Serialization.type =
    org.json4s.native.Serialization
  implicit val formats: DefaultFormats.type = org.json4s.DefaultFormats

  def getCenterIds(): Seq[Int] = {
    config.getIntList("centerIds").map(x => x.toInt).toSeq
  }

  def retrieveData(): Future[Seq[Any]] = {
    logger.info("Starting data retrieval")
    Future.sequence(
      getCenterIds()
        .map { id: Int =>
          logger.info(s"Sending request for centerId: $id")
          retrieveData(id)
        }
        .map {
          _.map {
            case (_, Left(exception)) =>
              logger.error("Got exception when retrieving data", exception)
            case (id, Right(response)) =>
              logger.info(s"Storing data in db")
              loadHourService.save(id, response.payload).recover { t: Throwable =>
                logger.error("Error when saving to db", t)
              }
          }
        }
    )
  }

  private def retrieveData(
      centerId: Int
  ): Future[(Int, Either[Exception, CenterLoadResponse])] = {
    val uri = uri"https://hfnapi.sats.com/satsweb/sats/centerload/$centerId"
    val request = basicRequest
      .get(uri)
      .response(asJson[CenterLoadResponse])
    request.send(backend)
      .map(response => (centerId, response.body))
  }

}
