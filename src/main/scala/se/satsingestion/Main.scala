package se.satsingestion

import akka.Done
import akka.actor.ActorSystem
import com.typesafe.config.ConfigFactory
import org.slf4j.{Logger, LoggerFactory}
import slick.jdbc.PostgresProfile
import sttp.client3._
import sttp.client3.akkahttp._

import scala.concurrent.duration.DurationInt
import scala.concurrent.{Await, ExecutionContextExecutor, Future}

object Main extends App {

  private implicit val logger: Logger = LoggerFactory.getLogger("SatsIngestion")

  private implicit val actorSystem: ActorSystem = ActorSystem()
  private val scheduler = actorSystem.scheduler
  private implicit val executor: ExecutionContextExecutor =
    actorSystem.dispatcher

  private val config =
    ConfigFactory.parseResources("application.conf").resolve()

  private val database = PostgresProfile.backend.Database.forConfig("db")
  private val loadHoursRepository: LoadHoursRepository =
    new LoadHoursRepository(database)

  private val backend: SttpBackend[Future, Any] = AkkaHttpBackend()

  private val loadHourService =
    new LoadHourService(logger, loadHoursRepository)

  private val dataRetriever =
    new DataRetriever(config, backend, loadHourService)

  loadHourService.dataRetriever = Some(dataRetriever)

  private val dataRetrievalScheduler = new DataRetrievalScheduler(
    config,
    dataRetriever,
    scheduler
  )

  Await.result(
    loadHoursRepository.createSchemaIfNotExists().recover {
      case e: Exception =>
        logger.info("createSchemaIfNotExists failed, assuming it already exists.")
        Done
    },
    10.minutes
  )
  dataRetrievalScheduler.schedule()

  private val loadHourApi =
    new LoadHourApi(config, dataRetriever, loadHourService)

  val bindingFuture = loadHourApi.bind()

  actorSystem.registerOnTermination {
    bindingFuture.flatMap(_.unbind())
    logger.info("closing http backend")
    backend.close()
    logger.info("closing database")
    database.close
  }

}
