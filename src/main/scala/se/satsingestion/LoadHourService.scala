package se.satsingestion

import java.sql.Timestamp
import java.time.{LocalDateTime, ZoneOffset}

import org.slf4j.Logger
import se.satsingestion.LoadHours.LoadHourRow

import scala.concurrent.{ExecutionContext, Future}

class LoadHourService(logger: Logger, loadHoursRepository: LoadHoursRepository)(
    implicit val executionContext: ExecutionContext) {

  var dataRetriever: Option[DataRetriever] = None

  def save(centerId: Int, payload: CenterLoadPayload, now: LocalDateTime = LocalDateTime.now(ZoneOffset.UTC)): Future[Seq[Int]] = {
    val hours = mapToRows(centerId, payload, now)
    loadHoursRepository.save(hours)
  }

  def list(): Future[Seq[LoadHour]] = {
    loadHoursRepository
      .list()
      .map(rows => rows.map(mapToEntity))
      .flatMap { result: Seq[LoadHour] =>
        refetchIfStale(result, loadHoursRepository.list)
      }
  }

  def list(centerId: Int): Future[Seq[LoadHour]] = {
    loadHoursRepository
      .list(centerId)
      .map(res => res.map(mapToEntity))
  }

  def listCurrentDay(): Future[Map[Int, Seq[LoadHour]]] = {
    val today = LocalDateTime.now(ZoneOffset.UTC).toLocalDate
    logger.info(s"listCurrentDay ($today)")
    loadHoursRepository
      .listCurrentDay()
      .map(rows => rows.map(mapToEntity))
      .flatMap { result: Seq[LoadHour] =>
        refetchIfStale(result, loadHoursRepository.listCurrentDay)
      }
      .map { (hours: Seq[LoadHour]) =>
        hours
          .filter(hour => hour.date == today && hour.actualLoadFactor.isDefined)
          .groupBy(hour => hour.centerId)
      }
  }

  private def refetchIfStale(hours: Seq[LoadHour],
                             refetchFunc: () => Future[Seq[LoadHourRow]]): Future[Seq[LoadHour]] = {
    if (hours.isEmpty || shouldRefetch(hours)) {
      dataRetriever.get.retrieveData().flatMap { _ =>
        refetchFunc()
          .map(rows => rows.map(mapToEntity))
      }
    } else {
      Future {
        hours
      }
    }
  }

  private def shouldRefetch(hours: Seq[LoadHour]): Boolean = {
    val lastSeen = hours
      .filter(_.actualLoadFactor.isDefined)
      .maxBy(hour => (hour.date, hour.hourNumber))
    logger.info("" + lastSeen)
    val lastSeenDate = lastSeen.date
    val lastSeenHour = lastSeen.hourNumber
    val now = LocalDateTime.now(ZoneOffset.UTC)
    val currentHour = now.getHour
    logger.info(
      s"currentHour: $currentHour lastSeenHour: $lastSeenHour currentDate: ${now.toLocalDate} lastseenDate: $lastSeenDate")
    lastSeenDate
      .isBefore(now.toLocalDate) || lastSeenDate == now.toLocalDate && currentHour < 21 && lastSeenHour < currentHour
  }

  private def mapToRows(centerId: Int, payload: CenterLoadPayload, now: LocalDateTime): Seq[LoadHourRow] = {
    val currentDate = now.toLocalDate
    val timestamp = Timestamp.valueOf(now)
    val date = Timestamp.valueOf(currentDate.atStartOfDay())

    if (payload.days.nonEmpty) {
      val day = payload.days.head
      day.hours
        .filter(_.actualLoadFactor.isDefined)
        .map { hour: HourWithLoad =>
          (centerId, timestamp, date, hour.hourNumber, hour.predictedLoadFactor, hour.actualLoadFactor)
        }
    } else {
      Seq.empty
    }
  }

  private def mapToEntity(
      row: (Int, Timestamp, Timestamp, Int, Float, Option[Float])
  ): LoadHour = {
    row match {
      case (centerId, createdAt, date, hourNumber, predictedLoadFactor, actualLoadFactor) =>
        LoadHour(centerId,
                 createdAt.toLocalDateTime,
                 date.toLocalDateTime.toLocalDate,
                 hourNumber,
                 predictedLoadFactor,
                 actualLoadFactor)
    }
  }

}
