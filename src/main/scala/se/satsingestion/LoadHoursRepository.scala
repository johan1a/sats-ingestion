package se.satsingestion

import org.slf4j.Logger
import se.satsingestion.LoadHours.LoadHourRow
import slick.jdbc.PostgresProfile
import slick.jdbc.PostgresProfile.api._
import slick.lifted.TableQuery

import scala.concurrent.{ExecutionContext, Future}

class LoadHoursRepository(database: PostgresProfile.backend.Database)(
    implicit val executionContext: ExecutionContext,
    logger: Logger) {

  val loadHours = TableQuery[LoadHours]

  def createSchemaIfNotExists(): Future[Unit] = {
    database.run(loadHours.schema.createIfNotExists)
  }

  def save(hours: Seq[LoadHourRow]): Future[Seq[Int]] = {
    Future.sequence(
      hours.map(row => database.run(loadHours.insertOrUpdate(row))))
  }

  def list(): Future[Seq[LoadHourRow]] = {
    database
      .run(
        loadHours
          .sortBy(row => (row.date, row.hourNumber))
          .result)
  }

  def list(centerId: Int): Future[Seq[LoadHourRow]] = {
    database
      .run(loadHours.filter(_.centerId === centerId).result)
  }

  def listCurrentDay(): Future[Seq[LoadHourRow]] = {
    database
      .run(
        loadHours
          .sortBy(row => (row.date, row.hourNumber))
          .result)
  }

}
