package se.satsingestion

import java.sql.Timestamp

import se.satsingestion.LoadHours.LoadHourRow
import slick.jdbc.PostgresProfile.api._

object LoadHours {

  type LoadHourRow = (Int, Timestamp, Timestamp, Int, Float, Option[Float])
}

class LoadHours(tag: Tag)
    extends Table[LoadHourRow](
      tag,
      "load_hours") {

  def centerId = column[Int]("center_id")

  def createdAt = column[Timestamp]("created_at")

  def date = column[Timestamp]("date")

  def hourNumber = column[Int]("hour_number")

  def predictedLoadFactor = column[Float]("predicted_load_factor")

  def actualLoadFactor = column[Option[Float]]("actual_load_factor")

  def pk = primaryKey("pk", (centerId, date, hourNumber))

  def * =
    (centerId,
     createdAt,
     date,
     hourNumber,
     predictedLoadFactor,
     actualLoadFactor)
}
