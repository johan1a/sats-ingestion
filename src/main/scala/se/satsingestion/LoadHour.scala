package se.satsingestion

import java.time.{LocalDate, LocalDateTime}

case class LoadHour(centerId: Int,
                    createdAt: LocalDateTime,
                    date: LocalDate,
                    hourNumber: Int,
                    predictedLoadFactor: Float,
                    actualLoadFactor: Option[Float])
