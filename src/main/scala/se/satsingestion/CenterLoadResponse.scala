package se.satsingestion

import java.time.LocalDateTime

case class HourWithLoad(
    hourNumber: Int,
    predictedLoadFactor: Float,
    actualLoadFactor: Option[Float]
)

case class CenterLoadDay(
    date: String,
    dayOfWeek: String,
    hours: Seq[HourWithLoad]
)

case class CenterLoadPayload(days: Seq[CenterLoadDay])

case class CenterLoadResponse(payload: CenterLoadPayload)

case class CenterLoadDayView(
    date: LocalDateTime,
    dayOfWeek: String,
    hours: Seq[HourWithLoad]
)
case class CenterLoadView(
    id: Int,
    centerId: Int,
    createdAt: LocalDateTime,
    days: Seq[CenterLoadDay]
)
