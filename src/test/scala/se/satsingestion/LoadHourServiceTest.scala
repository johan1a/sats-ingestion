package se.satsingestion

import java.sql.Timestamp
import java.time.{LocalDate, LocalDateTime, ZoneOffset}

import org.scalamock.scalatest.MockFactory
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.must.Matchers
import org.scalatest.matchers.should.Matchers.convertToAnyShouldWrapper
import org.slf4j.Logger
import se.satsingestion.LoadHours.LoadHourRow

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext, ExecutionContextExecutor, Future}

class LoadHourServiceTest extends AnyFunSpec with MockFactory with Matchers {

  implicit val executionContext: ExecutionContextExecutor =
    ExecutionContext.global
  implicit val logger: Logger = stub[Logger]

  it("should be able to save records to repository") {
    val repository = stub[LoadHoursRepository]
    (repository.save _).when(*).returning(Future { Seq() })

    val service = new LoadHourService(logger, repository)

    val centerId = 566
    val now = LocalDateTime.now(ZoneOffset.UTC)
    val hours = Seq(HourWithLoad(13, 0.5f, Some(0.6f)))
    val centerLoadDay =
      CenterLoadDay(now.toLocalDate.toString, now.toString, hours)
    val payload = CenterLoadPayload(Seq(centerLoadDay))

    Await.result(service.save(centerId, payload, now), Duration.Inf)

    val nowTimestamp = Timestamp.valueOf(now)
    val expected: List[LoadHourRow] =
      List(
        (centerId,
         nowTimestamp,
         Timestamp.valueOf(now.toLocalDate.atStartOfDay()),
         hours.head.hourNumber,
         hours.head.predictedLoadFactor,
         hours.head.actualLoadFactor))
    (repository.save _).verify(expected)
  }

  it("should be able to list") {
    val repository = stub[LoadHoursRepository]

    val now = LocalDateTime.now(ZoneOffset.UTC)
    val nowTimestamp = Timestamp.valueOf(now)
    val centerId = 123
    val row0 = (centerId, nowTimestamp, nowTimestamp, now.getHour, 0.3f, Some(0.2f))
    val row1 = (centerId, nowTimestamp, nowTimestamp, now.getHour - 1, 0.2f, Some(0.1f))
    val hoursInDb: Seq[LoadHourRow] = Seq(row0, row1)

    (repository.list: () => Future[Seq[LoadHourRow]]).when().returning(Future(hoursInDb))

    val service = new LoadHourService(logger, repository)

    val result = Await.result(service.list(), Duration.Inf)

    val expected = Seq(LoadHour(centerId, now, now.toLocalDate, now.getHour, row0._5, row0._6),
                       LoadHour(centerId, now, now.toLocalDate, now.getHour - 1, row1._5, row1._6))
    result shouldBe expected
  }

  it("should be able to list by centerId") {
    val repository = stub[LoadHoursRepository]

    val now = LocalDateTime.now(ZoneOffset.UTC)
    val nowTimestamp = Timestamp.valueOf(now)
    val centerId = 123
    val row0 = (centerId, nowTimestamp, nowTimestamp, now.getHour, 0.3f, Some(0.2f))
    val row1 = (centerId, nowTimestamp, nowTimestamp, now.getHour - 1, 0.2f, Some(0.1f))
    val hoursInDb: Seq[LoadHourRow] = Seq(row0, row1)

    (repository.list(_: Int)).when(centerId).returning(Future(hoursInDb))

    val service = new LoadHourService(logger, repository)

    val result = Await.result(service.list(centerId), Duration.Inf)

    val expected = Seq(LoadHour(centerId, now, now.toLocalDate, now.getHour, row0._5, row0._6),
                       LoadHour(centerId, now, now.toLocalDate, now.getHour - 1, row1._5, row1._6))
    result shouldBe expected
  }

  it("should be able to list data from currentDay") {
    val repository = stub[LoadHoursRepository]

    val now = LocalDateTime.now(ZoneOffset.UTC)
    val nowTimestamp = Timestamp.valueOf(now)
    val centerId = 123
    val row0 = (centerId, nowTimestamp, nowTimestamp, now.getHour, 0.3f, Some(0.2f))
    val row1 = (centerId, nowTimestamp, nowTimestamp, now.getHour - 1, 0.2f, Some(0.1f))
    val row2 = (centerId, nowTimestamp, nowTimestamp, now.getHour - 2, 0.2f, None)
    val hoursInDb: Seq[LoadHourRow] = Seq(row0, row1, row2)

    (repository.listCurrentDay _).when().returning(Future(hoursInDb))

    val service = new LoadHourService(logger, repository)

    val result = Await.result(service.listCurrentDay(), Duration.Inf)

    val expected = Map(
      centerId -> Seq(LoadHour(centerId, now, now.toLocalDate, now.getHour, row0._5, row0._6),
                      LoadHour(centerId, now, now.toLocalDate, now.getHour - 1, row1._5, row1._6)))
    result shouldBe expected
  }

  it("list() should refetch if we don't have the latest data") {
    val repository = stub[LoadHoursRepository]

    val now = LocalDateTime.now(ZoneOffset.UTC)
    val nowTimestamp = Timestamp.valueOf(now)
    val centerId = 123
    val row = (centerId, nowTimestamp, nowTimestamp, now.getHour - 1, 0.2f, Some(0.1f))
    val hoursInDb: Seq[LoadHourRow] = Seq(row)

    (repository.list: () => Future[Seq[LoadHourRow]]).when().returning(Future(hoursInDb))

    val service = new LoadHourService(logger, repository)
    val dataRetriever = stub[DataRetriever]

    (dataRetriever.getCenterIds _).when().returning(Seq(123))
    (dataRetriever.retrieveData _).when().returning(Future { Seq.empty })

    service.dataRetriever = Some(dataRetriever)

    Await.result(service.list(), Duration.Inf)

    (dataRetriever.retrieveData _).verify()
  }

  it("list() should not refetch if we have the latest data") {
    val repository = stub[LoadHoursRepository]

    val now = LocalDateTime.now(ZoneOffset.UTC)
    val nowTimestamp = Timestamp.valueOf(now)
    val centerId = 123
    val row = (centerId, nowTimestamp, nowTimestamp, now.getHour, 0.2f, Some(0.1f))
    val hoursInDb: Seq[LoadHourRow] = Seq(row)

    (repository.list: () => Future[Seq[LoadHourRow]]).when().returning(Future(hoursInDb))

    val service = new LoadHourService(logger, repository)
    val dataRetriever = stub[DataRetriever]

    (dataRetriever.getCenterIds _).when().returning(Seq(123))
    (dataRetriever.retrieveData _).when().returning(Future { Seq.empty })

    service.dataRetriever = Some(dataRetriever)

    Await.result(service.list(), Duration.Inf)

    (dataRetriever.retrieveData _).verify().never()
  }

  it("listCurrentDay() should refetch if we don't have the latest data") {
    val repository = stub[LoadHoursRepository]

    val now = LocalDateTime.now(ZoneOffset.UTC)
    val nowTimestamp = Timestamp.valueOf(now)
    val centerId = 123
    val row = (centerId, nowTimestamp, nowTimestamp, now.getHour - 1, 0.2f, Some(0.1f))
    val hoursInDb: Seq[LoadHourRow] = Seq(row)

    (repository.listCurrentDay _).when().returning(Future(hoursInDb))

    val service = new LoadHourService(logger, repository)
    val dataRetriever = stub[DataRetriever]

    (dataRetriever.getCenterIds _).when().returning(Seq(123))
    (dataRetriever.retrieveData _).when().returning(Future { Seq.empty })

    service.dataRetriever = Some(dataRetriever)

    Await.result(service.listCurrentDay(), Duration.Inf)

    (dataRetriever.retrieveData _).verify()
  }

  it("listCurrentDay() should not refetch if we have the latest data") {
    val repository = stub[LoadHoursRepository]

    val now = LocalDateTime.now(ZoneOffset.UTC)
    val nowTimestamp = Timestamp.valueOf(now)
    val centerId = 123
    val row = (centerId, nowTimestamp, nowTimestamp, now.getHour, 0.2f, Some(0.1f))
    val hoursInDb: Seq[LoadHourRow] = Seq(row)

    (repository.listCurrentDay _).when().returning(Future(hoursInDb))

    val service = new LoadHourService(logger, repository)
    val dataRetriever = stub[DataRetriever]

    (dataRetriever.getCenterIds _).when().returning(Seq(123))
    (dataRetriever.retrieveData _).when().returning(Future { Seq.empty })

    service.dataRetriever = Some(dataRetriever)

    Await.result(service.listCurrentDay(), Duration.Inf)

    (dataRetriever.retrieveData _).verify().never()
  }
}
