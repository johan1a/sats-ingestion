package se.satsingestion

import java.time.{LocalDate, LocalDateTime}

import spray.json.DefaultJsonProtocol.{FloatJsonFormat, IntJsonFormat, jsonFormat6, _}
import spray.json.{JsString, JsValue, JsonFormat, RootJsonFormat}

trait JsonSupport {

  object LocalDateTimeFormat extends JsonFormat[LocalDateTime] {
    override def read(json: JsValue): LocalDateTime = {
      json match {
        case JsString(string) =>
          LocalDateTime.parse(string)
        case _ => throw new RuntimeException()
      }
    }

    override def write(obj: LocalDateTime): JsValue = JsString(obj.toString)
  }

  object LocalDateFormat extends RootJsonFormat[LocalDate] {
    override def read(json: JsValue): LocalDate =
      json match {
        case JsString(string) =>
          LocalDate.parse(string)
        case _ => throw new RuntimeException()
      }

    override def write(obj: LocalDate): JsValue = JsString(obj.toString)
  }

  implicit val localDateFormat: RootJsonFormat[LocalDate] = LocalDateFormat
  implicit val localDateTimeFormat: LocalDateTimeFormat.type = LocalDateTimeFormat

  implicit val loadHourFormat: RootJsonFormat[LoadHour] = jsonFormat6(LoadHour)

}
