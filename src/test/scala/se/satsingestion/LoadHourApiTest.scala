package se.satsingestion

import java.time.LocalDateTime

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import akka.http.scaladsl.testkit.ScalatestRouteTest
import com.typesafe.config.Config
import org.scalamock.scalatest.MockFactory
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.should.Matchers
import org.slf4j.{Logger, LoggerFactory}
import spray.json.DefaultJsonProtocol._
import scala.concurrent.duration._
import akka.http.scaladsl.testkit.RouteTestTimeout
import akka.testkit.TestDuration

import scala.concurrent.Future

class LoadHourApiTest extends AnyFunSpec with MockFactory with Matchers with ScalatestRouteTest with JsonSupport {

  implicit val logger: Logger = LoggerFactory.getLogger("LoadHourApiTest")
  implicit val timeout = RouteTestTimeout(5.seconds.dilated)

  it("It should be able to list hours") {
    val config = stub[Config]
    val dataRetriever = stub[DataRetriever]
    val loadHourService = stub[LoadHourService]
    val now = LocalDateTime.now
    val today = now.toLocalDate
    val hours =
      Seq(LoadHour(2910, now, today, 18, 0.2f, Some(0.3f)), LoadHour(2919, now.plusHours(1), today, 19, 0.35f, None))
    (loadHourService.list: () => Future[Seq[LoadHour]]).when().returning(Future { hours })

    val api = new LoadHourApi(config, dataRetriever, loadHourService)
    Get("/api/sats/hours") ~> api.route ~> check {
      responseAs[List[LoadHour]] shouldEqual hours
    }

  }

  it("It should be able to list hours for center") {
    val config = stub[Config]
    val dataRetriever = stub[DataRetriever]
    val loadHourService = stub[LoadHourService]
    val now = LocalDateTime.now
    val today = now.toLocalDate
    val centerId = 2910
    val hour0 = LoadHour(centerId, now, today, 18, 0.2f, Some(0.3f))
    val hours = Seq(hour0)
    (loadHourService.list (_: Int)).when(*).returning(Future { hours })

    val api = new LoadHourApi(config, dataRetriever, loadHourService)
    Get(s"/api/sats/hours/${centerId}") ~> api.route ~> check {
      responseAs[List[LoadHour]] shouldEqual Seq(hour0)
    }

  }

}
