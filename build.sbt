lazy val root = (project in file(".")).settings(
  inThisBuild(
    List(
      organization := "se.satsingestion",
      scalaVersion := "2.13.7"
    )
  ),
  name := "sats-ingestion"
)

assemblyJarName in assembly := "sats-ingestion.jar"

assemblyMergeStrategy in assembly := {
  case PathList("META-INF", xs @ _*) => MergeStrategy.discard
  case PathList("reference.conf")    => MergeStrategy.concat
  case x                             => MergeStrategy.first
}

val AkkaHttpVersion = "10.2.1"
val AkkaVersion = "2.6.10"
val SttpVersion = "3.0.0-RC6"
val SlickVersion = "3.3.3"
val testcontainersScalaVersion = "0.38.4"

libraryDependencies ++= Seq(
  "com.softwaremill.sttp.client3" %% "core" % SttpVersion,
  "com.softwaremill.sttp.client3" %% "slf4j-backend" % SttpVersion,
  "com.softwaremill.sttp.client3" %% "circe" % SttpVersion,
  "io.circe" %% "circe-generic" % "0.13.0",
  "com.softwaremill.sttp.client3" %% "json4s" % SttpVersion,
  "com.softwaremill.sttp.client3" %% "akka-http-backend" % SttpVersion,
  "org.json4s" %% "json4s-native" % "3.6.6",
  "ch.qos.logback" % "logback-classic" % "1.2.3",
  "com.typesafe.akka" %% "akka-slf4j" % AkkaVersion,
  "com.typesafe.akka" %% "akka-stream" % AkkaVersion,
  "com.typesafe.akka" %% "akka-http" % AkkaHttpVersion,
  "com.typesafe.akka" %% "akka-stream" % AkkaVersion,
  "com.typesafe.slick" %% "slick" % SlickVersion,
  "org.slf4j" % "slf4j-nop" % "1.6.4",
  "com.typesafe.slick" %% "slick-hikaricp" % SlickVersion,
  "org.postgresql" % "postgresql" % "9.4-1206-jdbc42",
  "com.typesafe.akka" %% "akka-http-spray-json" % AkkaHttpVersion,
  "com.dimafeng" %% "testcontainers-scala-scalatest" % testcontainersScalaVersion % Test,
  "com.dimafeng" %% "testcontainers-scala-mysql" % testcontainersScalaVersion % Test,
  "org.scalatest" %% "scalatest" % "3.2.2" % Test,
  "org.scalamock" %% "scalamock" % "4.4.0" % Test,
  "com.typesafe.akka" %% "akka-stream-testkit" % AkkaVersion % Test,
  "com.typesafe.akka" %% "akka-http-testkit" % AkkaHttpVersion % Test
)

scalacOptions ++= Seq(
)
